<?php
declare(strict_types=1);

namespace r418518\ocrypt;

use InvalidArgumentException;
use LengthException;

/**
 * Encryption using OpenSSL 
 * 
 * Plaintext is encrypted and decrypted using openssl_encrypt() and openssl_decrypt()
 * 
 * The openssl_encrypt() and openssl_decrypt() functions require the name of the cipher method to use and an intialization vector
 *
 * This class is just simplifying the process by storing the IV, and ciphertext in a single string
 * 
 * The final string will be:  base64encoded( IV + ciphertext from openssl_encrypt() )
 *
 * Decryption then involves decoding the string, and parsing substrings to get IV, and the ciphertext
 * 
 * Then openssl_decrypt() is used to get the original plaintext
 **/
class Ocrypt implements Encryption
{
    /**
     * The cipher method that will be used, (default AES-128-CBC)
     * 
     * @var string
     **/
    private $cipherMethod = 'AES-128-CBC';


    /**
     * The key to use to encrypt/decrypt
     *
     * @var string
     **/
    private $key;
       

    /**
     * Set key and cipher method
     * 
     * @param string $key
     * @param string $cipherMethod
     * @return void
     */
    public function __construct( string $key, string $cipherMethod = null )
    {
        $this->key = $key;

        if( null !== $cipherMethod )
        {
            $this->setCipherMethod($cipherMethod);
        }
    }


    /**
     * Set the cipher method to use for the openssl encryption
     * 
     * (Will throw an exception if the passed cipher method name is not available.)
     *
     * @param string $name 
     * @return void
     **/
    private function setCipherMethod( string $name ) : void
    {
        if( false === in_array($name, $this->getAvailableCipherMethods()) )
        {
            throw new InvalidArgumentException("Cipher method '$name' is not available", 1);
        }

        $this->cipherMethod = $name;
    }


    /**
     * Return the set cipher method
     *
     * @return string 
     **/
    public function getCipherMethod() : string 
    {
        return $this->cipherMethod;
    }


    /**
     * Return available cipher methods
     *
     * @return array
     **/
    public function getAvailableCipherMethods() : array
    {
        return openssl_get_cipher_methods();
    }


    /**
     * Return the required size of the initialization vector for cipher method
     *
     * @return int
     **/
    public function getIVLength() : int
    {
        return openssl_cipher_iv_length( $this->cipherMethod ); 
    }


    /**
     * Generate a random intialization vector to use
     *
     * @return string
     **/
    public function generateIV() : string 
    {
        return random_bytes( $this->getIVLength() );
    }


    /**
     * Generate a random base64 encoded key
     *
     * @return string 
     **/
    public function generateRandomKey() : string 
    {
        $length = $this->cipherMethod == 'AES-128-CBC' ? 16 : 32;

        return base64_encode( random_bytes( $length ) );
    }


    /**
     * Encrypt data
     *
     * (Will throw exception if the passed optional initialization vector is not the correct size for the cipher method.)
     * 
     * @param mixed $data
     * @param string $iv (optional)
     * @return string
     **/
    public function encrypt( $data, string $iv = null )
    {
        // if IV, check it is the correct number of bytes
        if( null !== $iv  &&  strlen($iv) != $this->getIVLength() )
        {
            throw new LengthException("Initialization vector must be exactly {$this->getIVLength()} bytes", 1);
        }

        // if no IV, generate one of correct size
        $iv = null !== $iv ?: $this->generateIV();

        $plaintext = serialize( $data );

        $ciphertext = openssl_encrypt( $plaintext, $this->cipherMethod, $this->key, 0, $iv );

        return base64_encode( $iv . $ciphertext );
    }


    /**
     * Decrypt to get original data
     *
     * @param string $string
     * @return mixed
     **/
    public function decrypt( string $string )
    {
        $decoded = base64_decode( $string );

        $iv = substr( $decoded, 0, $this->getIVLength() );

        $ciphertext = substr( $decoded, $this->getIVLength() );

        $plaintext = openssl_decrypt( $ciphertext, $this->cipherMethod, $this->key, 0, $iv );

        $data = unserialize( $plaintext );
        
        return $data;
    }

} 