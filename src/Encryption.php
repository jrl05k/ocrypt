<?php
declare(strict_types=1);

namespace r418518\ocrypt;

/**
 * OpenSSL encryption
 **/
interface Encryption
{
    /**
     * Encrypt data
     * 
     * @param mixed $data
     * @param string $iv (optional)
     * @return string
     **/
    public function encrypt( $data, string $iv = null );

    /**
     * Decrypt to get original data
     *
     * @param string $string
     * @return mixed
     **/
    public function decrypt( string $string );
}