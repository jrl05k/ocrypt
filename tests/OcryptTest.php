<?php
use PHPUnit\Framework\TestCase;
use r418518\ocrypt\Ocrypt;

class OcryptTest extends TestCase
{
    protected $key;
    protected $ocrypt;

    protected function setUp(): void
    {
        $this->key = base64_encode( random_bytes(16) );
        $this->ocrypt = new Ocrypt( $this->key );
    }


    /**
     * Test encrypting / decrypting string 
     *
     * @return void
     **/
    public function testEncryptDecryptString()
    {
        $data = 'abcdefghijklmnopqrstuvwxyz1234567890`~!@#$%^&*()_+=-|\;:",./<>?';

        $encrypted = $this->ocrypt->encrypt( $data );
        $decrypted = $this->ocrypt->decrypt( $encrypted );

        $this->assertNotEquals( $data, $encrypted );
        $this->assertSame( $data, $decrypted );
    }

    /**
     * Test encrypting / decrypting integer
     *
     * @return void
     **/
    public function testEncryptDecryptInteger()
    {
        $data = mt_rand();

        $encrypted = $this->ocrypt->encrypt( $data );
        $decrypted = $this->ocrypt->decrypt( $encrypted );

        $this->assertNotEquals( $data, $encrypted );
        $this->assertSame( $data, $decrypted );
    }

    /**
     * Test encrypting / decrypting array
     *
     * @return void
     **/
    public function testEncryptDecryptArray()
    {
        $data = ['foo', 'bar', 'baz'];

        $encrypted = $this->ocrypt->encrypt( $data );
        $decrypted = $this->ocrypt->decrypt( $encrypted );

        $this->assertNotEquals( $data, $encrypted );
        $this->assertSame( $data, $decrypted );
    }


    /**
     * Test encrypting / decrypting array
     *
     * @return void
     **/
    public function testEncryptDecryptObject()
    {
        $data = new stdClass;
        $data->id = '23982392392';

        $encrypted = $this->ocrypt->encrypt( $data );
        $decrypted = $this->ocrypt->decrypt( $encrypted );

        $this->assertNotEquals( $data, $encrypted );
        $this->assertEquals( $data, $decrypted );
    }

}
