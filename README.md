# Ocrypt
This package is just a wrapper for [openssl_encrypt()](https://www.php.net/manual/en/function.openssl-encrypt) / [openssl_decrypt()](https://www.php.net/manual/en/function.openssl-decrypt.php) that stores the initialization vector along with the encrypted data. 
